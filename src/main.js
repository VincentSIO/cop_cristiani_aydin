import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import {Utils} from './mixins/utils.js'
import axios from 'axios'

Vue.use(BootstrapVue);

Vue.config.productionTip = false


Vue.mixin(Utils)

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state));
});

window.axios = axios.create({
  baseURL: 'http://coop.api.netlor.fr/api',
  params : { token : false },
  headers: { Authorization : 'Token token=248b41df759045a8b0d15f61af192e52'}
});
/*c338605b678bccb94a7d07b03591d9b473fedc3d*/
new Vue({
  router,
  store,
  render: h => h(App),
  beforeCreate() {
    this.$store.commit('initialiseStore');
  }
}).$mount('#app')
